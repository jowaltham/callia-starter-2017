# Callia Starter 2017 Theme

## Installation Instructions

1. Upload the Callia Starter 2017 theme folder via FTP to your wp-content/themes/ directory. (The Genesis parent theme needs to be in the wp-content/themes/ directory as well.)
2. Go to your WordPress dashboard and select Appearance.
3. Activate the Callia Starter 2017 theme.
4. Inside your WordPress dashboard, go to Genesis > Theme Settings and configure them to your liking.


## Theme Support

Please visit https://www.calliaweb.com/ for theme support.
