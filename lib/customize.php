<?php
/**
 * Callia Starter 2017.
 *
 * This file adds the Customizer additions to the Callia Starter 2017 Theme.
 *
 * @package Callia Starter 2017
 * @author  CalliaWeb
 * @license GPL-2.0+
 * @link    https://www.calliaweb.co.uk/
 */

/**
 * Get default link color for Customizer.
 *
 * Abstracted here since at least two functions use it.
 *
 * @since 2.2.3
 *
 * @return string Hex color code for link color.
 */
function jmw_customizer_get_default_link_color() {
	return '#c3251d';
}

/**
 * Get default accent color for Customizer.
 *
 * Abstracted here since at least two functions use it.
 *
 * @since 2.2.3
 *
 * @return string Hex color code for accent color.
 */
function jmw_customizer_get_default_accent_color() {
	return '#c3251d';
}

add_action( 'customize_register', 'jmw_customizer_register' );
/**
 * Register settings and controls with the Customizer.
 *
 * @since 2.2.3
 *
 * @param WP_Customize_Manager $wp_customize Customizer object.
 */
function jmw_customizer_register() {

	global $wp_customize;

	$wp_customize->add_setting(
		'jmw_link_color',
		array(
			'default'           => jmw_customizer_get_default_link_color(),
			'sanitize_callback' => 'sanitize_hex_color',
		)
	);

	$wp_customize->add_control(
		new WP_Customize_Color_Control(
			$wp_customize,
			'jmw_link_color',
			array(
				'description' => __( 'Change the default color for linked titles, menu links, post info links and more.', 'callia-starter' ),
			    'label'       => __( 'Link Color', 'callia-starter' ),
			    'section'     => 'colors',
			    'settings'    => 'jmw_link_color',
			)
		)
	);

	$wp_customize->add_setting(
		'jmw_accent_color',
		array(
			'default'           => jmw_customizer_get_default_accent_color(),
			'sanitize_callback' => 'sanitize_hex_color',
		)
	);

	$wp_customize->add_control(
		new WP_Customize_Color_Control(
			$wp_customize,
			'jmw_accent_color',
			array(
				'description' => __( 'Change the default color for button hovers.', 'callia-starter' ),
			    'label'       => __( 'Accent Color', 'callia-starter' ),
			    'section'     => 'colors',
			    'settings'    => 'jmw_accent_color',
			)
		)
	);

}
