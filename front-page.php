<?php
/**
 * Callia Starter 2017.
 *
 * This file adds the front-page template to the Callia Starter 2017 Theme.
 *
 * @package Callia Starter 2017
 * @author  CalliaWeb
 * @license GPL-2.0+
 * @link    https://www.calliaweb.co.uk/
 */

// Make site title h1 for the front page
add_filter( 'genesis_site_title_wrap', 'jmw_h1_for_site_title' );
function jmw_h1_for_site_title( $wrap ) {
	return 'h1';
}

//* Force full-width-content layout
add_filter( 'genesis_pre_get_option_site_layout', '__genesis_return_full_width_content' );

//* Run the Genesis loop
genesis();
